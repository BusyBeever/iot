function initialize(){
	var canvas = document.getElementById("graphics");
	var ctx = canvas.getContext("2d");
	
	ctx.fillStyle = "#FFFF00";
	ctx.fillRect(50, 100, 380, 400);
	ctx.fillStyle = "rgba(0, 0, 128, 0.8)";
	ctx.fillRect(25, 50, 380, 500);
	
	var width = canvas.width;
	var height = canvas.height;
	
	var w = width/10;
	var h = height/10;
	ctx.fillStyle = "cyan";
	for(var i = 0; i < 10; i++){
		ctx.fillRect(i * w, i * h, w, h);
	}
	
	// Definition einer Konstruktorfunktion
	function Square(x, y, w){
		this.x = x;
		this.y = y;
		this.width = w;
		this.draw = function(){
			ctx.fillRect(this.x, this.y, this.width, this.width);
		}
		/* etwas ausführlicher
			function drawFunction(){...}
			this.draw = drawFunction;
		*/
	}
	// Verwendung der Konstruktorfunktion=Erzeugung eines Objekts
	var square1 = new Square(200, 300, 60);
	square1.x = 0;
	square1.draw();
	
}







