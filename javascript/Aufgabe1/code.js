function initialize(){
	var element = document.getElementById("very-large-label");
	element.style.color = "rgb(255, 0, 0)";
	element.style["background-color"] = "#0f0";

	var numberfield = document.getElementById("numberfield");

	function onSliderChanged(){
		/* wird beim Bewegen des Sliders ausgeführt*/
		console.log(this.value);
		
		numberfield.innerHTML = this.value;
	}
	
	var slider = document.getElementById("slider");
//	slider.addEventListener("change", onSliderChanged, false);
	slider.addEventListener("input", onSliderChanged, false);
	
	// Event-Listener für Mouse-Events
	function mouseHandler(e){
		var x = e.pageX;
		var y = e.pageY;
		var square = document.getElementById("square");
		square.style["left"] = x - 25 + "px";
		square.style["top"] = y - 25 + "px";
	}
	
	var container = document.getElementsByTagName("body")[0];
//	container.addEventListener("mousemove", mouseHandler);

	// Event-Listener für Touch-Events
	function touchHandler(e){
		var x = e.changedTouches[0].pageX;
		var y = e.changedTouches[0].pageY;
		var square = document.getElementById("square");
		square.style["left"] = x - 25 + "px";
		square.style["top"] = y - 25 + "px";
	}
	container.addEventListener("touchmove", touchHandler);
}