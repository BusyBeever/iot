package com.example.websocketclient;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

// see https://github.com/socketio/socket.io-client-java
public class MainActivity extends AppCompatActivity {
    public final static String LOCALHOST_FROM_EMULATOR = "http://10.0.2.2:3000/";
    public final static String LOCALHOST = "http://127.0.0.1:3000/";

    // -----> PLEASE CHANGE THIS
    public final static String HOST = LOCALHOST_FROM_EMULATOR;

    private SeekBar seekBar;
    private Socket socket;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        try {
            socket = IO.socket(HOST);
            socket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
            socket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
            socket.on("sliderChanged", onSliderChanged);
            socket.connect();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        seekBar = (SeekBar)findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(onSeekBarChangeListener);
    }
    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Toast.makeText(getApplicationContext(), "Connection Error",  Toast.LENGTH_LONG).show();
        }
    };
    private Emitter.Listener onSliderChanged = new Emitter.Listener(){
        @Override
        public void call(Object... args) {
            JSONObject obj = (JSONObject)args[0];
            try {
                int value = obj.getInt("value");
                Log.i("WEBSOCKETCLIENT", "sliderChanged");

                seekBar.setProgress(value);
            } catch (JSONException e) {
            }
        }
    };
    private SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("value", progress);
                socket.emit("sliderChanged", obj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        @Override
        public void onStartTrackingTouch(SeekBar seekBar) { }
        @Override
        public void onStopTrackingTouch(SeekBar seekBar) { }
    };
}
