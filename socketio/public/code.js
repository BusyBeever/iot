function initialize(){
	var socket = io.connect();
	
	socket.on("tick", function(data){
		var value = data.value;
		document.getElementById("label").innerHTML = value;
	});
	
	var slider = document.getElementById("slider");
	slider.addEventListener("input", function(){
		var value = this.value;
		socket.emit("sliderChanged", {"value": value});
	});
	
	socket.on("sliderChanged", function(data){
		slider.value = data.value;
	});
}