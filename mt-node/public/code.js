function initialize(){
	// stellt eine Verbindung zum Server her
	var socket = io.connect();
	
	// definiert Eventhandler f�r die Nachricht "hello"
	socket.on("hello", function(data){
		var label = document.getElementById("label");
		label.innerHTML = data.message;
	});
	
	// Eventhandler f�r "click" definieren und setzen
	var checkbox = document.getElementById("checkbox");
	checkbox.addEventListener("click", function(){
		if (this.checked){
			socket.emit("checkbox", {"value": 1});
		}
		else{
			socket.emit("checkbox", {"value": 0});
		}
	});
	
	// Nachricht "checkbox" vom Server empfangen
	socket.on("checkbox", function(data){
		if (data.value == 1){
			checkbox.checked = true;
		}
		else{
			checkbox.checked = false;
		}
	});
}