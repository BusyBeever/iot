// definiert den Port
var PORT = 3000;
// Initialisierung des Webservers
var express = require("express");
var app = express();
var server = require("http").Server(app);
// startet den Webserver
app.listen(PORT);

// Webserver f�r statische HTML-Seiten
app.use(express.static(__dirname + '/public'));

// definiert Antwort "Hello World" auf Anfrage "hello"
app.get('/hello', function(req, res){
	res.send("Hello World");
});

// definiert Antwort auf Request "info"
app.get('/info', function(req, res){
	res.send("<html>" +
			"<head><title>Info</title></head>" +
			"<body><h1>Info Jahresausstellung</h1></body>" + 
			"</html>");
});