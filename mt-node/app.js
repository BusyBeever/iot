// definiert den Port
var PORT = 3000;
// Initialisierung des Webservers
var express = require("express");
var app = express();
var server = require("http").Server(app);
var io = require("socket.io")(server);
server.listen(PORT);

// Webserver f�r statische HTML-Seiten
app.use(express.static(__dirname + '/public'));

// definiert Eventhandler f�r das Event "connection"
// d.h. ein Websocket-Client hat sich verbunden
io.sockets.on('connection', function(socket){
	console.log ("Client connected");
	
	// Nachricht an diesen Client senden
	socket.emit("hello", {"message": "hello Client"});
	
	// Nachricht "checkbox" empfangen
	socket.on("checkbox", function(data){
		console.log("Checkbox: " + data.value);
		socket.broadcast.emit("checkbox", data);
	});
});
