function initialize(){
	// holt das Element mit id="info"
	var helloText = document.getElementById("info");
	
	// setzt den Text auf "Hallo ITS"
	helloText.innerHTML = "Hallo ITS";
	
	// setzt Hintgrundfarbe auf rot
	helloText.style["background-color"] = "red";
	
	//Aufgabe: setzen die Font-Size auf 48 Pixel
	helloText.style["font-size"] = "48px";
	
	// Definition Event-Listener
	function onSliderChanged(){
		// aktuellen Wert des Sliders holen
		var sliderValue = this.value;
		// diesen Wert in das Label "slider-value" schreiben
		var sliderValueLabel = document.getElementById("slider-value");
		sliderValueLabel.innerHTML = sliderValue;
		// Font-Gr��e ver�ndern
		helloText.style["font-size"] = sliderValue + "px";
	}
	// diese Funktion als Eventlistener des Sliders setzen
	var slider = document.getElementById("test-slider");
	slider.addEventListener("input", onSliderChanged);
	
}