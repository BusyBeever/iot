var PORT = 3000;
var http = require('http');

// Request-Eventhandler
function requestHandler(request, response){
	response.writeHead(200, {'Content-Type': 'text/html'});
//	response.write("Hello World!");
	response.write("<html>"
		+ "<head><title>Antwort von Node</title></head>"
		+ "<body>Hello world"
		+ "<input type='range'>"
		+ "</body>"
		+ "</html>");
	response.end();
}

// Request-Handler setzen und Server starten
var httpServer = http.createServer(requestHandler);
httpServer.listen(PORT);